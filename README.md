# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 3.1.0

* System dependencies

* Running the Server
Firstly run `bundle install` to install the gems. After that, just run `rails s`

* Database creation
`rake db:create` then `rake db:migrate`

* How to run the test suite
Just run `rspec`

* Services (job queues, cache servers, search engines, etc.)
Run `bundle exec sidekiq` to enable background jobs. *Required for notifications delivery*

* Deployment instructions

* ...
