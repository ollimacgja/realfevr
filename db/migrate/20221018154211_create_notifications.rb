class CreateNotifications < ActiveRecord::Migration[7.0]
  def change
    create_table :notifications do |t|
      t.text :message
      t.references :player, null: false, foreign_key: true
      t.integer :status, default: 0
      t.boolean :sent, default: false
      

      t.timestamps
    end
  end
end
