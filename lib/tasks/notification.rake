desc "Remove all notifications that are older than 7 days"
task notification_cleanup: :environment do
  Notification.for_deletion.destroy_all
end