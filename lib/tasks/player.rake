desc "Updates players data from external data"
task update_players: :environment do
  file = File.read('lib/files/players.json') #This can be changed to read a api endpont in the future
  data = JSON.parse(file, symbolize_names: true)[:data]
  data[:teams].each do |team_data|
    team = Team.where(name: team_data[:name], acronym: team_data[:acronym]).first_or_initialize
    team.save
    team_data[:players].each do |player_data|
      player = team.players.where(name: player_data[:name], nationality: player_data[:nationality], birthdate: Date.parse(player_data[:birthdate])).first_or_initialize
      player.assign_attributes(number: player_data[:number], position: player_data[:position])
      player.save
    end
  end
end