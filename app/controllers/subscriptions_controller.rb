class SubscriptionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_player

  def create
    if @player.nil? || current_user.players.include?(@player)
      render json: { errors: I18n.t('.already_subscribed') }, status: :unprocessable_entity
    else
      current_user.players << @player 
      render json: { message: I18n.t('.subscription_created') }, status: :created
    end
  end

  def destroy
    if !@player.nil? && current_user.players.include?(@player)
      current_user.players.delete(@player)
    end
    
  end
  
  private
  def set_player
    @player = Player.find_by_id(params[:player_id])
  end  
end
