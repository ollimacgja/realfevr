class Notification < ApplicationRecord
  validates :message, :status, presence: true
  enum status: [ :draft, :published ]

  belongs_to :player

  scope :for_deletion, -> { where('created_at < ?', 7.days.ago) }

  after_save :trigger_notification, if: :should_send_notification?

  def trigger_notification
    NotificationJob.perform_later(self)
  end

  def should_send_notification?
    !sent? && published?
  end
end
