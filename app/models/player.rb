class Player < ApplicationRecord
  validates :name, :position, :birthdate, :nationality, presence: true

  has_and_belongs_to_many :users
  belongs_to :team, optional: true
end
