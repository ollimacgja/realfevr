class Team < ApplicationRecord
  validates :name, :acronym, presence: true
  validates :acronym, length: { is: 3 }

  has_many :players, dependent: :nullify
end
