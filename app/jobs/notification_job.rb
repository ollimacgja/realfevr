class NotificationJob < ApplicationJob
  queue_as :default

  def perform(notification, user_id=nil)
    if user_id.present?
      NotificationMailer.notification_email(notification.id, user_id).deliver_now
    else
      notification.player.users.pluck(:id).each do |user_id|
        NotificationJob.perform_later(notification, user_id)
      end
      notification.update(sent: true)
    end
  end
end
