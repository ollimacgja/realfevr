class NotificationMailer < ApplicationMailer
  default from: "realfevr@challenge.com"

  def notification_email(notification_id, user_id)
    @user = User.find(user_id)
    @notification = Notification.find(notification_id)
    @player = @notification.player

    mail(to: @user.email, subject: "[RealFevr]Notification regarding player #{@player.name}")
  end
  
end
