require "rails_helper"

RSpec.describe NotificationMailer, type: :mailer do
  describe 'notification_email' do
    let(:user) { create(:user) }
    let(:notification) { create(:notification) }
    let(:player) { notification.player }
    let(:mail) { described_class.notification_email(notification.id, user.id).deliver_now }

    it 'renders the subject' do
      expect(mail.subject).to eq("[RealFevr]Notification regarding player #{player.name}")
    end

    it 'renders the receiver email' do
      expect(mail.to).to eq([user.email])
    end

    it 'renders the sender email' do
      expect(mail.from).to eq(['realfevr@challenge.com'])
    end
  end
end
