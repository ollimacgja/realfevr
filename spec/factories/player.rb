FactoryBot.define do
  factory :player do
    name { "Roberto Carlos" }
    number { 6 }
    nationality { "Brazil" }
    birthdate { Date.new(1973,04,10) }
    position { "D" }
  end
end