FactoryBot.define do
  factory :team do
    name { "MyString" }
    acronym { "MyString" }
  end
end
