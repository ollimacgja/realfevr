FactoryBot.define do
  factory :notification do
    message { "MyText" }
    player factory: :player
    status { "draft" }
  end
end
