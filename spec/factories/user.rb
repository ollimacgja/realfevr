FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "user-#{n.to_s.rjust(3, "0")}@email.com"}
    password { "12345678" }
  end
end