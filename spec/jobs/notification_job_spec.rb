require 'rails_helper'

RSpec.describe NotificationJob, type: :job do
  ActiveJob::Base.queue_adapter = :test
  
  let(:notification) { create(:notification)}
  let(:player) { notification.player }
  let!(:user) { create(:user, players: [player]) }

  describe "#perform_later" do
    context "without user_id params" do
      it "triggers a new job with the user id" do
        expect {
          NotificationJob.perform_now(notification)
        }.to have_enqueued_job.with(notification, user.id)
      end      
    end

    context "with user_id params" do
      it "delivers a notification emails" do
        NotificationJob.perform_now(notification, user.id)
        email = ActionMailer::Base.deliveries.last
        expect(email.to).to eq([user.email])
      end      
    end
    
  end
end
