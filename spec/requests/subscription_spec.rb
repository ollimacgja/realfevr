require 'rails_helper'
require 'devise/jwt/test_helpers'

RSpec.describe "/subscription", type: :request do
  let(:player) { create(:player) }
  let(:user) { create(:user) }
  
  let(:valid_attributes) {
    {
      player_id: player.id
    }
  }

  let(:invalid_attributes) {
    {
      player_id: nil
    }
  }

  let(:valid_headers) do
    
    Devise::JWT::TestHelpers.auth_headers({}, user)
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new subscription" do
        expect {
          post subscription_url,
               params: valid_attributes, headers: valid_headers, as: :json
        }.to change(user.players, :count).by(1)
      end

      it "renders a JSON response with the new subscription" do
        post subscription_url,
             params: valid_attributes, headers: valid_headers, as: :json
        expect(response).to have_http_status(:created)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end

    context "with invalid parameters" do
      it "does not create a new subscription" do
        expect {
          post subscription_url,
               params: invalid_attributes, as: :json
        }.to change(user.players, :count).by(0)
      end

      it "renders a JSON response with errors for the new subscription" do
        post subscription_url,
             params: invalid_attributes, headers: valid_headers, as: :json
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response.content_type).to match(a_string_including("application/json"))
      end
    end
  end

  describe "DELETE /destroy" do
    before(:each) { user.players << player}
    context "with valid parameters" do
      it "removes subscription" do
        expect {
          delete subscription_url,
               params: valid_attributes, headers: valid_headers, as: :json
        }.to change(user.players, :count).by(-1)
      end
    end

    context "with invalid parameters" do
      it "does not create a new subscription" do
        expect {
          delete subscription_url,
               params: invalid_attributes, as: :json
        }.to change(user.players, :count).by(0)
      end
    end
  end
end
