require "rails_helper"
Rails.application.load_tasks

describe "notification.rake" do
  context ".notification_cleanup" do
    let!(:notification_1) {create(:notification, created_at: 10.days.ago)}
    let!(:notification_2) {create(:notification, created_at: 6.days.ago)}

    it "should delete only the first notification" do
      Rake::Task["notification_cleanup"].invoke
      expect(Notification.count).to eq(1)
      expect(Notification.all).not_to include(notification_1)
      expect(Notification.all).to include(notification_2)
    end
  end
end