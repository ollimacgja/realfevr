require 'rails_helper'

RSpec.describe Notification, type: :model do
  it { should validate_presence_of(:message) }
  it { should validate_presence_of(:status) }

  it { should belong_to(:player) }

  describe "Scopes" do
    context ".for_deletion" do
      let!(:notification_1) {create(:notification, created_at: 10.days.ago)}
      let!(:notification_2) {create(:notification, created_at: 6.days.ago)}

      it "should select only the first notification" do
        expect(Notification.for_deletion.count).to eq(1)
        expect(Notification.for_deletion).to include(notification_1)
        expect(Notification.for_deletion).not_to include(notification_2)
      end

    end
  end

  describe "After_save" do
    ActiveJob::Base.queue_adapter = :test
    describe "#trigger_notification" do
      context "Published and not sent notification" do
        let(:notification) { build(:notification, status: "published")}
        it "should trigger a background job" do
          expect {
            notification.save
          }.to have_enqueued_job
        end        
      end      

      context "Draft and not sent notification" do
        let(:notification) { build(:notification, status: "draft")}
        it "should not trigger a background job" do
          expect {
            notification.save
          }.not_to have_enqueued_job
        end        
      end      
      
      context "Published and sent notification" do
        let(:notification) { build(:notification, status: "published", sent: true)}
        it "should not trigger a background job" do
          expect {
            notification.save
          }.not_to have_enqueued_job
        end        
      end      
    end    
  end  
end
