require 'rails_helper'

RSpec.describe Team, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:acronym) }
  it { should validate_length_of(:acronym).is_equal_to(3) }
  it { should have_many(:players) }
end
