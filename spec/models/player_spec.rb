require 'rails_helper'

RSpec.describe Player, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:position) }
  it { should validate_presence_of(:birthdate) }
  it { should validate_presence_of(:nationality) }

  it { should have_and_belong_to_many(:users) }
  it { should belong_to(:team).optional }
end
